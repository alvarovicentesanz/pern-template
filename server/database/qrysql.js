const qrysl = {
    INSERT: 'INSERT INTO kwtable (kwfield_ej) VALUES($1) RETURNING *',
    SELECT_ALL: 'SELECT * FROM kwtable',
    SELECT_ONE: 'SELECT * FROM kwtable WHERE kwfield_id = ($1)',
    UPDATE: 'UPDATE kwtable SET kwfield_ej = ($1) WHERE kwfield_id = ($2) RETURNING *',
    DELETE: 'DELETE FROM kwtable WHERE kwfield_id = ($1)'
}
module.exports = qrysl