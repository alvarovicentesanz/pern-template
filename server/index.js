const express = require('express')
const cors = require('cors')
const app = express()
const pool = require('./database/db')
const {
    INSERT,
    SELECT_ALL,
    SELECT_ONE,
    UPDATE,
    DELETE
} = require('./database/qrysql')

//middleware
app.use(cors())
app.use(express.json())

//ROUTES//

//CRUD//
app.post("/kwtable", async (req, res) => {
    try {
        const { kwfield_ej } = req.body
        
        const newRecord = await pool.query(INSERT,[kwfield_ej])
        res.json(newRecord.rows[0])
    }
    catch (err) {
        console.log(err.message)
    }
})

app.get("/kwtable", async (req, res) => {
    try {
        const newRecord = await pool.query(SELECT_ALL)
        res.json(newRecord.rows)
    }
    catch (err) {
        console.log(err.message)
    }
})

app.get("/kwtable/:id", async (req, res) => {
    try {
        const { id } = req.params
        const newRecord = await pool.query(SELECT_ONE , [id])
        res.json(newRecord.rows[0])
    }
    catch (err) {
        console.log(err.message)
    }
})

app.put("/kwtable/:id", async (req, res) => {
    try {
        const { kwfield_ej } = req.body
        const { id } = req.params

        const newRecord = await pool.query(UPDATE , [kwfield_ej , id])
        res.json(newRecord.rows[0])
    }
    catch (err) {
        console.log(err.message)
    }
})

app.delete("/kwtable/:id", async (req, res) => {
    try {
        const { id } = req.params

        const newRecord = await pool.query(DELETE , [id])
        res.json(newRecord.rowCount)
    }
    catch (err) {
        console.log(err.message)
    }
})

//var port = process.env.PORT

var server = app.listen(5000, () => {
    console.log('server on port ', server.address().port)
})